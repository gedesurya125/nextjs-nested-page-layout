import React from "react";

// External Components
import { Box, Heading } from "@gedesurya125/surya-ui";

export const Header = () => {
  return (
    <Box
      as="header"
      sx={{
        bg: "blue",
        color: "white",
        p: "2rem",
      }}
    >
      <Title />
    </Box>
  );
};

const Title = () => {
  return (
    <Heading
      as="h1"
      sx={{
        fontSize: "3rem",
        fontFamily: "heading",
      }}
    >
      Dashboard with nested route
    </Heading>
  );
};
