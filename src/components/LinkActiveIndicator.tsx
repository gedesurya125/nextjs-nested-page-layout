import React from "react";

// External Components
import { Box } from "@gedesurya125/surya-ui";
import type { ThemeUIStyleObject } from "theme-ui";

interface ActiveLinkIndicatorProps {
  layoutId: string;
  sx?: ThemeUIStyleObject;
}

export const ActiveLinkIndicator = ({
  layoutId,
  sx,
}: ActiveLinkIndicatorProps) => {
  return (
    <Box
      className="active-indicator"
      layoutId={layoutId}
      sx={{
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        bg: "white",
        ...sx,
      }}
    ></Box>
  );
};
