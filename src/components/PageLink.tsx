/** @jsxImportSource theme-ui */

import React from "react";

import Link from "next/link";
import { motion } from "framer-motion";
import { ThemeUIStyleObject } from "theme-ui";

// hooks
// import { useLocation } from '@reach/router';

const MotionNextLink = motion(Link);

export interface PageLinkProps {
  to: string;
  children: React.ReactNode;
  variant?: "pageLink";
  sx?: ThemeUIStyleObject;
  className?: string;
}

export const PageLink = ({
  to = "/",
  children,
  variant = "pageLink",
  className = "",
  sx,
}: PageLinkProps) => {
  return (
    <MotionNextLink
      className={className}
      href={to}
      sx={{
        variant: `links.${variant}`,
        display: "block",
        ...sx,
      }}
    >
      {children}
    </MotionNextLink>
  );
};
