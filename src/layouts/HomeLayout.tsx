import React from "react";

// External Components
import { Box } from "@gedesurya125/surya-ui";
import Link from "next/link";
import { PageLink, PageLinkProps } from "src/components/PageLink";
import { useRouter } from "next/router";
import { ActiveLinkIndicator } from "src/components";

interface HomeLayoutProps {
  children: React.ReactNode;
}

export const HomeLayout = ({ children }: HomeLayoutProps) => {
  console.log("this is the children", children);
  return (
    <>
      <NavigationBar />
      {children}
    </>
  );
};

const NavigationBar = () => {
  const availableLinks = [
    { label: "Home", to: "/" },
    { label: "Pages", to: "/pages" },
    {
      label: "Settings",
      to: "/settings",
    },
  ];

  return (
    <Box
      sx={{
        bg: "teal",
        color: "white",
        p: "1rem 4rem",
        display: "flex",
      }}
    >
      {availableLinks.map((link, index) => (
        <LinkItem key={index} to={link.to}>
          {link.label}
        </LinkItem>
      ))}
    </Box>
  );
};

const LinkItem = ({ children, to }: PageLinkProps) => {
  const router = useRouter();

  const pathList = router.pathname.split("/");

  const currentPath = (pathList: string[]): string => {
    const pathToCheck = [...pathList];
    if (!pathToCheck[pathToCheck.length - 1].includes("["))
      return pathToCheck[pathToCheck.length - 1];
    if (pathToCheck.length > 1) {
      pathToCheck.pop();
      return currentPath(pathToCheck.slice(0));
    }
    return "";
  };

  const isActive = currentPath(pathList) === to.replace("/", "");

  return (
    <PageLink
      className="navigation-link"
      to={to}
      sx={{
        transition: "all 0.4s ease",
        color: isActive ? "black" : "white",
        "& ~ .navigation-link": {
          ml: ["3rem"],
        },
        position: "relative",
        p: "1rem",
      }}
    >
      <Box
        as="span"
        sx={{
          position: "relative",
          zIndex: 1,
        }}
      >
        {children}
      </Box>
      {isActive && (
        <ActiveLinkIndicator layoutId="navigation-link-active-indicator" />
      )}
    </PageLink>
  );
};
