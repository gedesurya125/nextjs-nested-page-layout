import React from "react";

// Local Components
import { Header, Footer } from "src/components";

interface MainLayout {
  Component: any;
  pageProps: any;
}

export const MainLayout = ({ Component, pageProps }: MainLayout) => {
  const getLayout = Component.getLayout || ((page: React.ReactElement) => page);

  return (
    <>
      <Header />
      {getLayout(<Component {...pageProps} />)}
      <Footer />
    </>
  );
};
