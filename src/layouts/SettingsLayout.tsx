import React from "react";

// External Components
import { Box } from "@gedesurya125/surya-ui";
import { ActiveLinkIndicator, PageLink, PageLinkProps } from "src/components";
import { useRouter } from "next/router";

// Framer Motion

export const SettingsLayout = ({
  children,
}: {
  children: React.ReactElement;
}) => {
  const { number } = children.props;
  return (
    <Box
      sx={{
        display: "grid",
        gridTemplateColumns: ["15% 1fr"],
        minHeight: "80vh",
      }}
    >
      <SideBar number={number} />
      <Box as="main">{children}</Box>
    </Box>
  );
};

const SideBar = ({ number }: { number: number }) => {
  const availableLinks = [
    {
      label: "Setting 1",
      to: "/settings/1",
    },
    {
      label: "Setting 2",
      to: "/settings/2",
    },
  ];

  return (
    <Box
      sx={{
        bg: "teal",
        height: "100%",
      }}
    >
      {availableLinks.map((link, index) => {
        return (
          <OptionLink
            isActive={link.to === `/settings/${number}`}
            key={index}
            to={link.to}
          >
            {link.label}
          </OptionLink>
        );
      })}
    </Box>
  );
};

interface OptionLinkProps extends PageLinkProps {
  isActive: boolean;
}

const OptionLink = ({ to, children, isActive }: OptionLinkProps) => {
  const router = useRouter();

  console.log("this is the path name", router.pathname);

  return (
    <PageLink
      to={to}
      sx={{
        position: "relative",
        fontFamily: "body",
        color: isActive ? "black" : "white",
        transition: "all 0.5s ease",
        p: "1rem",
      }}
    >
      {isActive && (
        <ActiveLinkIndicator layoutId="setting-link-active-indicator" />
      )}
      <Box
        as="span"
        sx={{
          zIndex: 1,
          position: "relative",
        }}
      >
        {children}
      </Box>
    </PageLink>
  );
};
