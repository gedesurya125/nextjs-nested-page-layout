import "src/styles/globals.css";
import type { AppProps } from "next/app";
import { ThemeProvider } from "@gedesurya125/surya-ui";

import { MainLayout } from "src/layouts";
import { theme } from "src/theme";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={theme}>
      <MainLayout Component={Component} pageProps={pageProps} />
    </ThemeProvider>
  );
}
