import Head from "next/head";

// Local Components

import { HomeLayout } from "src/layouts";

export default function Home() {
  return (
    <>
      <Head>
        <title>Nested Route Dashboard</title>
        <meta
          name="description"
          content="This is simple dashboard that implement nested route"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      Hello form index
    </>
  );
}

// Until here
export const getHomePageLayout = (page: React.ReactElement) => {
  return <HomeLayout>{page}</HomeLayout>;
};

Home.getLayout = (page: React.ReactElement) => {
  return getHomePageLayout(page);
};
