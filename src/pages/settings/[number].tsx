import type { GetStaticPropsContext } from "next";
import React from "react";
import { getSettingPageLayout } from ".";

interface SettingItemProps {
  number: number;
}

const SettingItem = ({ number }: SettingItemProps) => {
  console.log("this is the number", number);
  return <div>SettingItem {number}</div>;
};

export default SettingItem;
SettingItem.getLayout = getSettingPageLayout;

export const getStaticProps = async ({ params }: GetStaticPropsContext) => {
  return {
    props: {
      ...params,
    },
    revalidate: 1,
  };
};
export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: "blocking",
  };
};

//  The benefit using this layout pattern is we get persistent page state (input value, scroll position, etc) for a SPA experience
// resources https://nextjs.org/docs/basic-features/layouts
