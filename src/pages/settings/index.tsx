import React from "react";

import { HomeLayout, SettingsLayout } from "src/layouts";
import { getHomePageLayout } from "..";

const Settings = () => {
  return <div>Hello from setting</div>;
};

export default Settings;

export const getSettingPageLayout = (page: React.ReactElement) => {
  return (
    <HomeLayout>
      <SettingsLayout>{page}</SettingsLayout>
    </HomeLayout>
  );
};

Settings.getLayout = getSettingPageLayout;
