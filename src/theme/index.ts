import { Theme } from "theme-ui";
import { Inter } from "@next/font/google";

const inter400 = Inter({
  subsets: ["latin"],
  weight: "400",
  fallback: ["sans"],
});
const inter700 = Inter({
  subsets: ["latin"],
  weight: "500",
  fallback: ["sans"],
});

export const theme: Theme = {
  fonts: {
    heading: inter700.style.fontFamily,
    body: inter400.style.fontFamily,
  },
  links: {
    pageLink: {
      fontFamily: "body",
      fontSize: ["1.8rem"],
      textDecoration: "none",
      cursor: "pointer",
    },
  },
  colors: {
    primary: "blue",
    secondary: "teal",
    text: "black",
  },
};
